<?php
/**
Plugin Name: stclub-fetch-api
description: create table from user api data
Version: 1.0
Author: Nadeem bhatti
License: MIT
Text Domain: stclub-fetch-api
*/

// If this file is accessed directory, then abort.
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

define('BASE_PATH', plugin_dir_path(__FILE__));
// include the Composer autoload file
require BASE_PATH . 'vendor/autoload.php';

use Stclub\StclubFetchApi\AssetsLoader; 
use Stclub\StclubFetchApi\ShortCode;  
use Stclub\StclubFetchApi\RoutsApi;   

add_action( 'plugins_loaded', 'stclub_fetch_user_api' );
function stclub_fetch_user_api() {
	$stc_loader = new AssetsLoader\StclubAssetsLoader();
	$stc_loader->init();
	$stc_api_routes	= new RoutsApi\StclubRestRoutsApi();
	$stc_api_routes->init();
	$stc_table_shortcode = new ShortCode\StclubShortcode();
	$stc_table_shortcode->init();
}