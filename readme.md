Fetch api
    Fetch api plugin developed by stclub pvt Ltd. Its used for fetching data via api using HTTP request.
    Fetch api plugin show user info briefly in html table and click any cell in table then complete info of user
    will be show in another table.
How to use
    Run command 'composer require stclub/stclub-fetch-api:dev-master' in command line. Plugin will download with 
    wp-content folder so copy 'stclub-fetch-api' folder and place it your worpress plugin directory.
    Activate plugin and paste short code in new page.
    short code is [stc_api_table] save page and visit the page you will see endpoint button click button and see desire
    result in table like in screenshot
    https://drive.google.com/file/d/1zkdhvyHV3NlwXcE5IrbZgQo_Pc_eWrhz/view?usp=sharing
    
Requirements
    composer
    Latest version of wordpress >= 5.3
    php version >= 7.2
Path
    default project name is wordpress for this plugin, if your project name is different or you use it at live server then 
    change the end_url variable path in js file. you can see js file in 'stclub-fetch-api/public/js/my-ajax-request.js.