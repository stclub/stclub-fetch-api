jQuery(document).ready(function($){
    
    $('#callapi').click(function(e){
    	var api_url = 'https://jsonplaceholder.typicode.com/users';
    	var end_url = window.location.origin+'/wordpress/wp-json/api/all-user';
    	jQuery.ajax({
		    url: end_url,
		    beforeSend: function() {
	    		$("#loader").show();
	  		},
	  		success: function(result){
	  			if(result == null || result == undefined){
	  				alert("Apologies! We could not find the API. Please try again later");
	  				return false;
	  			}
	  			if ( $("#response_area").is( ".stc_api" ) ) {
		    		alert("User info table already created");
		    		return false;
		    	}
				
				$("#response_area").addClass("stc_api");
			            var count=0;
		        $("#response_area").append("<tr><th>id</th><th>name</th><th>username</th><th>email</th></tr>");
		        $.each(result, function () {
		    	
			    	$("#response_area").append("<tr><td><a href='#' class='stc_service' id="+result[count].id+">"
			    		+result[count].id+"</a></td><td><a href='#' class='stc_service' id="+result[count].id+">"
			    		+result[count].name+"</a></td><td><a href='#' class='stc_service' id="+result[count].id+">"
			    		+result[count].username+"</a></td><td>"
			    		+result[count].email+"</td></tr>"
			    	);
		    	
		    		count++;
		    	});
		    	$(".stc_service").click(function(e) {
		        	e.preventDefault();
		    		id=this.id;
			    	$.ajax({
				        url: api_url+'/'+id,
				        contentType: "application/json",
				        dataType: 'json',
				        beforeSend: function() {
			    			$("#loader").show();
			  			},
				        success: function(user_info){
				        		if(user_info == null || result == undefined){
					  				alert("Apologies! We could not find the API. Please try again later");
					  				return false;
					  			}
				        		$("#user_info").html("<tr><th>id</th><th>name</th><th>username</th><th>email</th><th>address</th><th>phone</th><th>website</th><th>company</th></tr><tr><td>"
				        			+user_info.id+"</td><td>"
				        			+user_info.name+"</td><td>"
				        			+user_info.username+"</td><td>"
				        			+user_info.email+"</td><td>"
				        			+user_info.address.street+" "+user_info.address.suit+" "+user_info.address.city+"</td><td>"
				        			+user_info.phone+"</td><td>"
				        			+user_info.website+"</td><td>"
				        			+user_info.company.name+"</td></tr>"
				        		);	
				        	
				        },
		        		complete:function(){
		    				// Hide image container
		    				$("#loader").hide();
		        		}
		    		});
		    
				});
		
	  		},
            complete:function(){	
    			$("#loader").hide();
        	}
        });

	});
});    	