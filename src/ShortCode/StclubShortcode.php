<?php
namespace Stclub\StclubFetchApi\ShortCode;
class StclubShortcode {
     
    public function init() {
		// register shortcode

		add_shortcode('stc_api_table', array(__CLASS__, 'stc_shortcode'));
	}
	// function that runs when shortcode is called
	function stc_shortcode() { 
	?>
		<button type="button" id="callapi" class="btn btn-info">End Point</button>
		<div id="response_area"></div>
  		<div id="loader" style="display:none;"></div>
  		<div id="user_info"></div> 
		
	<?php
	} 
	
}