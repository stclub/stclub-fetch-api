<?php
namespace Stclub\StclubFetchApi\AssetsLoader;
class StclubAssetsLoader implements StclubAssetsInterface {

	public function init() {

		add_action( 'wp_enqueue_scripts', array(__CLASS__, 'enqueue') );

	}

	/**
	 * Defines the functionality responsible for loading the file.
	 */
	function enqueue(){
	
	    wp_register_style( 'stclub_fetch_api', plugins_url('stclub-fetch-api') . '/public/assets/css/api_style.css' );
	    wp_enqueue_style( 'stclub_fetch_api');
	    wp_enqueue_script( 'my-ajax-handle', plugins_url('stclub-fetch-api') . '/public/assets/js/my-ajax-request.js', array( 'jquery' ) );
	    
	}
	
}