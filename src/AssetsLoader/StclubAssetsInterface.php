<?php
namespace Stclub\StclubFetchApi\AssetsLoader;
/**
 * Defines a common set of functions that any class responsible for loading
 * stylesheets, JavaScript, or other assets should implement.
 */
interface StclubAssetsInterface {
	public function init();
	public function enqueue();
}