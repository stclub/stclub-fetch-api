<?php
namespace Stclub\StclubFetchApi\RoutsApi;
class StclubRestRoutsApi {
	
	public function init() {
		add_action( 'rest_api_init', array(__CLASS__, 'stc_register_route'));
	}
	function stc_register_route() {
	    register_rest_route( 'api', 'all-user', array(
	                    'methods' => 'GET',
	                    'callback' => array(__CLASS__, 'api_data')
	                )
	            );
	}
	function api_data() {
		$stc_api_info = get_transient( 'stc_info' );
	 
		if( false === $stc_api_info ) {
			
		    // Transient expired, refresh the data
		    $request = wp_remote_get('https://jsonplaceholder.typicode.com/users');
			$body = wp_remote_retrieve_body( $request );
			$data = json_decode($body, true);
			set_transient( 'stc_info', $body, 60 );
			
		}else {
			
			$data = json_decode($stc_api_info, true);
		}
		
		return rest_ensure_response($data);
	}
}